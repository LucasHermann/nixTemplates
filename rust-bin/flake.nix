{
  description = "falke template for Rust projects";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    crane = {
      url = "github:ipetkov/crane";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
        rust-overlay.follows = "rust-overlay";
      };
    };
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, crane }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };

        craneLib = crane.lib.${system};
        src = craneLib.cleanCargoSource builtins.path { path = ./.; name = "CHANGE_ME"; };

        # Build Cargo dependencies
        cargoArtifacts = craneLib.buildDepsOnly {
          inherit src;
        };

        # Build binary
        cargoBinary = craneLib.buildPackage {
          inherit cargoArtifacts src;

          doCheck = true;
        };

        app = flake-utils.lib.mkApp {
          drv = cargoBinary;
        };

        name = "CHANGE_ME";
      in rec {
        packages = {
          default = packages.${name};
          ${name} = cargoBinary;
        };

        apps = {
          default = apps.${name};
          ${name} = app;
        };

        devShells = {
          default = devShells.${name};
          ${name} = pkgs.mkShell {
            buildInputs = builtins.attrValues {
              inherit (pkgs)
              openssl
              pkg-config;
              inherit (pkgs.rust-bin.stable.latest)
              default;
            };
          };
        };
      }
    );
}
