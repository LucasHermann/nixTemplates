{
  description = " My personnal set of Flake Templates";

  outputs = { self }: {
    templates = {
      python = {
        description = "General Python template";
        path = ./python;
      };
      erlang = {
        description = "General Erlang template";
        path = ./erlang;
      };
      rust = {
        description = "Binary Rust template";
        path = ./rust-bin;
      };
    };
  };
}

